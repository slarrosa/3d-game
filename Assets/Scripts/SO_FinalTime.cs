﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_FinalTime : ScriptableObject
{
    public float finalTime;
}
