﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Win : MonoBehaviour
{
    public SO_FinalTime finalTime;
    void Start()
    {
        finalTime.finalTime = 0;    
    }

    void Update()
    {
        this.transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
        finalTime.finalTime += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Ball")
        {
            SceneManager.LoadScene("Final");
        }
    }
}
