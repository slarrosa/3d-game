﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class BallController : MonoBehaviour
{
    public SO_BallStats ballstats;
    public SO_Battery battery;

    private Rigidbody rb;
    private float movementX;
    private float movementY;
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        battery.remainingTime = 15;
    }


    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);
        rb.AddForce(movement * ballstats.ballspeed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag=="AliventWall" || collision.transform.tag == "Void")
        {
            SceneManager.LoadScene("Level1");
        }
    }
}