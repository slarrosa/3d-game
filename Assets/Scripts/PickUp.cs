﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public SO_Battery battery;
    void Update()
    {
        this.transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Ball")
        {
            this.gameObject.SetActive(false);
            battery.remainingTime += 10f;
        }
    }
}
