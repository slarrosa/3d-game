﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    public GameObject ball;
    private Vector3 offset;
    public SO_Battery battery;
    private bool blink = false;

    void Start()
    {
        offset = this.transform.position - ball.transform.position;
 
    }

    //Al update definim quan parpadeja la llum de la bola
    void Update()
    {

        if (battery.remainingTime >= 0f)
        {
            battery.remainingTime -= Time.deltaTime;
        }
        if (battery.remainingTime >= 5f)
        {
            this.GetComponent<Light>().range = 25;
        }
        else if (battery.remainingTime<=5f && battery.remainingTime >= 0f)
        {
            StartCoroutine(Blinking());
        }
        if (battery.remainingTime <= 0f)
        {
            this.GetComponent<Light>().range = 0;
        }
        
    }
    //LateUpdate començara despres de que tots els altres updates hagin començat
    //Fem que la posicio de la llum queddes com si fos filla de ball però sense agafar la rotació per que no giri.
    void LateUpdate()
    {
        this.transform.position = ball.transform.position + offset;
    }

    //Aquesta corrutina fa que la llum de la bola parpadeji al quedar menys de 6 segons de bateria
    IEnumerator Blinking()
    {
        if (!blink)
        {
            blink = true;
            this.GetComponent<Light>().range = 0;
            yield return new WaitForSeconds(0.5f);
            this.GetComponent<Light>().range = 25;
            yield return new WaitForSeconds(0.5f);
            blink = false;
        }
        yield return null;
    }

}
