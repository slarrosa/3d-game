﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasBattery : MonoBehaviour
{
    public SO_Battery battery;

    void Update()
    {
        GetComponent<TMPro.TextMeshProUGUI>().SetText("Battery time: " + battery.remainingTime.ToString("F1"));
    }
}
