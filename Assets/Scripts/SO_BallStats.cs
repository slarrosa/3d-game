﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_BallStats : ScriptableObject
{
    public float ballspeed;
}
