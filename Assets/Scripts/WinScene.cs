﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScene : MonoBehaviour
{
    public SO_FinalTime finalTime;
    void Start()
    {
        GetComponent<TMPro.TextMeshProUGUI>().SetText("Final time: " + finalTime.finalTime.ToString("F1") + " seconds");
    }
}
