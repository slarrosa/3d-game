﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject ball;
    private Vector3 offset;

    void Start()
    {
        offset = this.transform.position - ball.transform.position;
    }

    //LateUpdate començara despres de que tots els altres updates hagin començat
    //Fem que la posicio de la camara queddes com si fos filla de ball però sense agafar la rotació per que no giri.
    void LateUpdate()
    {
        this.transform.position = ball.transform.position + offset;
    }
}
