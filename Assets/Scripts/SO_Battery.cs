﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class SO_Battery : ScriptableObject
{
    public int battery;
    public float remainingTime;
}
